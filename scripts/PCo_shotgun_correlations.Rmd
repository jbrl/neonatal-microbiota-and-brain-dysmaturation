---
title: "TEBC pre-discharge faecal samples: correlations between principal coordinates and the abundance of species and gut-brain/gut-metabolic modules from shotgun metagenomic sequencing"
author: 
  - "Kadi Vaher"
  - "PCo-s calculated in the 16S_ordination script"
date: '`r Sys.Date()`'
output: html_document
---

```{r setup, include=F, message=F}

library(tidyverse) 
library(here) 
library(phyloseq)
#library(decontam)
library(RColorBrewer)
library(knitr)
library(ggpubr)
# library(vegan)
# library(ggrepel)
# library(ape)

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev = c('png'), 
                      fig.path = here("results", "figures", "PCo_shotgun_correlations", ""), dpi = 300)
theme_set(theme_light()) # global option for ggplot2
```

```{r knit, echo=F, eval=FALSE}

# Run the whole document - ONLY AT THE END OF THE WORKSTREAM

rmarkdown::render(input = here("scripts", "PCo_shotgun_correlations.Rmd"), output_dir = here("results", "reports"))

```

# Load functions

Here load some custom functions which support the analysis below. The scripts for these functions can be found in the `src/`-folder.
The load_dada is for creating the ASV names, filtering out 'Mitochondria', 'Chloroplast', 'Archae' or 'Eukaryota'

```{r}
source(here("src", "load_dada.R"))
source(here("src", "utils_kadi.R"))

```


# Load files

```{r readfiles, message=F}

#file with the PCO-s (the same used for bacteria-brain associations)
Pcos <- read.csv(here("processed_data", "matchingsample_pcoa_relativeabundances.csv"))

nrow(Pcos)

#shotgun phyloseq objects for the species-level data
phy_species <- readRDS(here("processed_data", "shotgun", "taxonomy_phyloseq", "phy_species_final.Rds"))

ntaxa(phy_species)
nsamples(phy_species)


```

# Wrangle to get them into the same dataframes

```{r wrangle, message=FALSE}

#take out only the PCo axes
Pcos <- Pcos %>% select(X, Axis.1, Axis.2, Axis.3, Axis.4)

Pcos <- Pcos %>% rename(sample_work_nr = X)

demo_matching <- read.csv(here("processed_data", "demographics_microbiome_mri_79_10102022.csv"))
demo_final_samples <- read.csv(here("processed_data", "demographics_alphadiversity_finalsamples_04102022.csv"))

alpha <- demo_final_samples %>% select(sample_work_nr, Observed_rarefied, Shannon_rarefied)

pcos_alpha <- merge(alpha, Pcos, by="sample_work_nr", all.y=T)

#select important demographics info (that will use in models)
imp_demo <- demo_matching %>% select(record_id, sample_id, sample_work_nr, Time_point, Visit_number, GA_birth, extremely_preterm, postnatal_age_at_sample_d, PMA_sample_w, sex, GA_mri, birthweight_z_score, sepsis_binary, nec_binary, Vaginal_delivery, AB_after72h, BM_75)

data <- merge(imp_demo, pcos_alpha, by="sample_work_nr")

#get the sample id-s (remember that in shotgun sequencing we had to rename the sample IDs, but they have already been matched in the phyloseq object)
sample_ids <- data.frame(sample_data(phy_species))
sample_ids <- sample_ids %>% 
  subset(Visit_number=="V2") %>%
  select(sample_id2, Novogene_ID, record_id)

#subset to MRI-matching dataset
matching_samples <- sample_ids %>% subset(record_id %in% data$record_id)
head(matching_samples)

#take out the species relative abundance dataframe: subset to matching dataframe and remove species not present
phy_species <- phy_species %>% subset_samples((Visit_number=="V2" & preterm=="1")) %>% 
  subset_samples((record_id %in% data$record_id)) %>%
  prune_taxa(taxa_sums(.) > 0, .)

ntaxa(phy_species)
nsamples(phy_species)

species <- as(otu_table(phy_species), "matrix")
species <- as.data.frame(t(species))

species <- merge(matching_samples, species, by=0, all.x = T)
species <- species %>% select(-Row.names)

#merge pco-s and species into one dataframe
data <- data %>% select(-sample_work_nr, -sample_id)
data_species <- merge(data, species, by="record_id", all.y=T)

#how does it look like
head(data_species)

```

# Correlations of the PCO-s and species

```{r cor_species, message=F, ow="50%"}

#correlation matrix between the axes and RA of species
cor_mx_species=cor(data_species[,18:21],data_species[,24:ncol(data_species)], method = "spearman")

#transform the correlation matrix and make into a dataframe
cor_mx_t_species <- t(cor_mx_species) %>% data.frame()

cor_mx_data_species <- cor_mx_t_species %>% mutate(Species=row.names(cor_mx_t_species)) %>% mutate(Species_name = format_OTU2(Species))

#put this into long format
library(reshape)
cor_mx_data_melt_species <- melt(cor_mx_data_species, id.vars = c("Species", "Species_name"), measure.vars = 1:4, variable_name="PCo")
detach("package:reshape", unload=TRUE)

cor_mx_data_melt_species <- cor_mx_data_melt_species %>% mutate(pos_neg = ifelse(value<0, "negative", "positive"))
cor_mx_data_melt_species$abs_value <- abs(cor_mx_data_melt_species$value)

a1 <- cor_mx_data_melt_species %>% subset(PCo=="Axis.1") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(Species_name, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=16), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Species") + xlab("Spearman correlation coefficient") +
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a2 <- cor_mx_data_melt_species %>% subset(PCo=="Axis.2") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(Species_name, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=16), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Species") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a3 <- cor_mx_data_melt_species %>% subset(PCo=="Axis.3") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(Species_name, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=16), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Species") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a4 <- cor_mx_data_melt_species %>% subset(PCo=="Axis.4") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(Species_name, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=16), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Species") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))


ggarrange(a1, a2, a3, a4, nrow=1, labels = c("PCo1, 15.05%", "PCo2, 13.22%", "PCo3, 8.07%", "PCo4, 4.53%"))

joint_species <- ggarrange(a1, a2, a3, a4, nrow=2, ncol=2, labels = c("PCo1, 15.05%", "PCo2, 13.22%", "PCo3, 8.07%", "PCo4, 4.53%"), font.label = list(size = 18), hjust = -0.2, vjust = 1.1)

joint_species

png(file=here("results", "figures", "pcoa_correlations_species_27042023.png"), width = 17, height = 19, units = "in",res=300)
print(joint_species)
dev.off()

tiff(file=here("results", "figures", "pcoa_correlations_species_27042023.tiff"), width = 17, height = 19, units = "in",res=300)
print(joint_species)
dev.off()

```

# PCo correlations with the gut-brain modules

First need to do some wrangling to put into the same dataset as the PCo-s

```{r gbm_wrangle, message=F}

#read in the gut-brain modules file
gbms <- read.csv(here("processed_data", "shotgun", "functional_other", "GBMs_KEGG_RA.csv"))
gbms <- gbms %>% column_to_rownames("X")


#transform to have modules as columns
gbms <- as.data.frame(t(gbms))

#subset to the final MRImatching dataset
gbms <- gbms %>% subset(row.names(.) %in% row.names(matching_samples))

#exclude modules not present 
gbms_filtered <- gbms[, colSums(gbms != 0) > 0]

#how many are left
ncol(gbms_filtered)

#add to the dataset with demographics
#first need to create a new column with the novogene sample name
gbms_filtered <- gbms_filtered %>% mutate(Novogene_ID=row.names(gbms_filtered))

#then take out the sample names together with study record-id-s from the species dataset
ids <- data_species %>% select(record_id, Novogene_ID)
data <- merge(data, ids, by="record_id")

#now merge with the gbm dataset
data_gbms <- merge(data, gbms_filtered, by="Novogene_ID")

head(data_gbms)

```

Then do a figure showing the most abundant GBMs in the MRI-matching dataset
```{r abundant_gbms, message=F, ow="50%"}

gbms_temp <- gbms_filtered %>% select(-Novogene_ID)
#transform the gbm dataframe into the original format (GBMs as rows)
gbms_temp <- as.data.frame(t(gbms_temp))

#calculate mean abundances and prevalence
mean_GBMs_matching <- data.frame(GBM =row.names(gbms_temp),
                        mean_abundance=rowMeans(gbms_temp),
                        zeros = rowSums(gbms_temp==0),
                        prevalence = 1- rowSums(gbms_temp==0)/ncol(gbms_temp))


mean_gbms_matching <- mean_GBMs_matching %>%
    ggplot(aes(x = mean_abundance, y = reorder(GBM, mean_abundance), fill=prevalence)) +
    geom_col(color = "grey10", position = "dodge", alpha=0.8) +
    labs(x = "Mean relative abundance", y = "", fill = "") +
  scale_x_continuous(labels = scales::percent, breaks=seq(0,0.0001,0.00005), limits=c(0, 0.0001)) +
  scale_fill_continuous(high = "#132B43", low = "#56B1F7") +
  theme_bw(base_size=13) +
  labs(fill="Prevalence")

save(mean_gbms_matching, file = here("results", "fig_objects", "mean_gbms_matching.rdata"))

png(file=here("results", "figures", "mean_gbms_matching.png"), width = 12, height = 10, units = "in",res=300)
print(mean_gbms_matching)
dev.off()

tiff(file=here("results", "figures", "mean_gbms_matching.tiff"), width = 12, height = 10, units = "in",res=300)
print(mean_gbms_matching)
dev.off()

```


```{r gbm_cor, message=F, ow="50%"}
#correlation matrix between the axes and RA of gbms
cor_mx_gbms=cor(data_gbms[,19:22],data_gbms[,23:ncol(data_gbms)], method = "spearman")

#transform the correlation matrix and make into a dataframe
cor_mx_t_gbms <- t(cor_mx_gbms) %>% data.frame()

cor_mx_data_gbms <- cor_mx_t_gbms %>% mutate(gbms=row.names(cor_mx_t_gbms))

#put this into long format
library(reshape)
cor_mx_data_melt_gbms <- melt(cor_mx_data_gbms, id.vars = c("gbms"), measure.vars = 1:4, variable_name="PCo")
detach("package:reshape", unload=TRUE)

cor_mx_data_melt_gbms <- cor_mx_data_melt_gbms %>% mutate(pos_neg = ifelse(value<0, "negative", "positive"))
cor_mx_data_melt_gbms$abs_value <- abs(cor_mx_data_melt_gbms$value)

a1_gbms <- cor_mx_data_melt_gbms %>% subset(PCo=="Axis.1") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gbms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  #theme(axis.text.y = ggtext::element_markdown(size=14), legend.position="none", axis.title.x = element_text(size = 16),
  #      axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  theme_bw(base_size=13) +
  theme(legend.position = "none") +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut-brain module") + xlab("Spearman correlation coefficient") +
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a2_gbms <- cor_mx_data_melt_gbms %>% subset(PCo=="Axis.2") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gbms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  #theme(axis.text.y = ggtext::element_markdown(size=14), legend.position="none", axis.title.x = element_text(size = 16),
  #      axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  theme_bw(base_size=13) +
  theme(legend.position = "none") +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut-brain module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a3_gbms <- cor_mx_data_melt_gbms %>% subset(PCo=="Axis.3") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gbms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  #theme(axis.text.y = ggtext::element_markdown(size=14), legend.position="none", axis.title.x = element_text(size = 16),
  #      axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  theme_bw(base_size=13) +
  theme(legend.position = "none") +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut-brain module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a4_gbms <- cor_mx_data_melt_gbms %>% subset(PCo=="Axis.4") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gbms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
 # theme(axis.text.y = ggtext::element_markdown(size=14), legend.position="none", axis.title.x = element_text(size = 16),
  #      axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  theme_bw(base_size=13) +
  theme(legend.position = "none") +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut-brain module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))


#ggarrange(a1_gbms, a2_gbms, a3_gbms, a4_gbms, nrow=1, labels = c("PCo1, 15.05%", "PCo2, 13.22%", "PCo3, 8.07%", "PCo4, 4.53%"))

joint_gbms <- ggarrange(a1_gbms, a2_gbms, a3_gbms, a4_gbms, nrow=2, ncol=2, labels = c("PCo1, 15.05%", "PCo2, 13.22%", "PCo3, 8.07%", "PCo4, 4.53%"), font.label = list(size = 14), hjust = -0.2, vjust = 1.1)

#joint_gbms

# png(file=here("results", "figures", "pcoa_correlations_gbms_27062023.png"), width = 19, height = 19, units = "in",res=300)
# print(joint_gbms)
# dev.off()
# 
# tiff(file=here("results", "figures", "pcoa_correlations_gbms_27062023.tiff"), width = 19, height = 19, units = "in",res=300)
# print(joint_gbms)
# dev.off()

#save as rdata object in order to put into a larger multipanel plot
save(joint_gbms, file = here("results", "fig_objects", "pcoa_correlations_gbms_smallertext.rdata"))

```

## Version 2 with smaller text

```{r smaller}

a1_gbms <- cor_mx_data_melt_gbms %>% subset(PCo=="Axis.1") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gbms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=14), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut-brain module") + xlab("Spearman correlation coefficient") +
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a2_gbms <- cor_mx_data_melt_gbms %>% subset(PCo=="Axis.2") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gbms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=14), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut-brain module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a3_gbms <- cor_mx_data_melt_gbms %>% subset(PCo=="Axis.3") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gbms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=14), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut-brain module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a4_gbms <- cor_mx_data_melt_gbms %>% subset(PCo=="Axis.4") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gbms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=14), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut-brain module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))


ggarrange(a1_gbms, a2_gbms, a3_gbms, a4_gbms, nrow=1, labels = c("PCo1, 15.05%", "PCo2, 13.22%", "PCo3, 8.07%", "PCo4, 4.53%"))

joint_gbms <- ggarrange(a1_gbms, a2_gbms, a3_gbms, a4_gbms, nrow=2, ncol=2, labels = c("PCo1, 15.05%", "PCo2, 13.22%", "PCo3, 8.07%", "PCo4, 4.53%"), font.label = list(size = 18), hjust = -0.2, vjust = 1.1)

joint_gbms

#save as rdata object in order to put into a larger multipanel plot
save(joint_gbms, file = here("results", "fig_objects", "pcoa_correlations_gbms_v2.rdata"))

```

# PCo correlations with the gut metabolic modules

```{r cor_gmm, message=F, ow="50%"}

#read in the gut metabolic modules file

gmms <- read.csv(here("processed_data", "shotgun", "functional_other", "GMMs_KEGG_RA.csv"))
gmms <- gmms %>% column_to_rownames("X")


#transform
gmms <- as.data.frame(t(gmms))

#subset to the final MRImatching dataset
gmms <- gmms %>% subset(row.names(.) %in% row.names(matching_samples))

#exclude pathways not present 
gmms_filtered <- gmms[, colSums(gmms != 0) > 0]

#add to the big dataset
gmms_filtered <- gmms_filtered %>% mutate(Novogene_ID=row.names(gmms_filtered))

data_gmms <- merge(data, gmms_filtered, by="Novogene_ID")


#correlation matrix between the axes and RA of gbms
cor_mx_gmms=cor(data_gmms[,19:22],data_gmms[,23:ncol(data_gmms)], method = "spearman")

#transform the correlation matrix and make into a dataframe
cor_mx_t_gmms <- t(cor_mx_gmms) %>% data.frame()

cor_mx_data_gmms <- cor_mx_t_gmms %>% mutate(gmms=row.names(cor_mx_t_gmms))

#put this into long format
library(reshape)
cor_mx_data_melt_gmms <- melt(cor_mx_data_gmms, id.vars = c("gmms"), measure.vars = 1:4, variable_name="PCo")
detach("package:reshape", unload=TRUE)

cor_mx_data_melt_gmms <- cor_mx_data_melt_gmms %>% mutate(pos_neg = ifelse(value<0, "negative", "positive"))
cor_mx_data_melt_gmms$abs_value <- abs(cor_mx_data_melt_gmms$value)

a1_gmms <- cor_mx_data_melt_gmms %>% subset(PCo=="Axis.1") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gmms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=10), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut metabolic module") + xlab("Spearman correlation coefficient") +
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a2_gmms <- cor_mx_data_melt_gmms %>% subset(PCo=="Axis.2") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gmms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=10), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut metabolic module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a3_gmms <- cor_mx_data_melt_gmms %>% subset(PCo=="Axis.3") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gmms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=10), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut metabolic module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))

a4_gmms <- cor_mx_data_melt_gmms %>% subset(PCo=="Axis.4") %>% slice_max(abs_value, n=20) %>%
  ggplot(aes(x=value,y=reorder(gmms, -value), fill=pos_neg))+ 
               geom_bar(stat = "identity" ,width = 0.7) +
  theme(axis.text.y = ggtext::element_markdown(size=10), legend.position="none", axis.title.x = element_text(size = 16),
        axis.title.y = element_text(size = 16), axis.text.x = element_text(size=12)) +
  scale_fill_manual(values=c("#34487A", "#CF5A47"), name="") +
  ylab("Gut metabolic module") + xlab("Spearman correlation coefficient")+
  scale_x_continuous(breaks=seq(-0.8,0.8,0.2), limits=c(-0.8, 0.8))


ggarrange(a1_gmms, a2_gmms, a3_gmms, a4_gmms, nrow=1, labels = c("PCo1, 15.05%", "PCo2, 13.22%", "PCo3, 8.07%", "PCo4, 4.53%"))

joint_gmms <- ggarrange(a1_gmms, a2_gmms, a3_gmms, a4_gmms, nrow=2, ncol=2, labels = c("PCo1, 15.05%", "PCo2, 13.22%", "PCo3, 8.07%", "PCo4, 4.53%"), font.label = list(size = 18), hjust = -0.2, vjust = 1.1)

joint_gmms

png(file=here("results", "figures", "pcoa_correlations_gmms_27042023.png"), width = 19, height = 19, units = "in",res=300)
print(joint_gmms)
dev.off()

```
