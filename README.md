# Neonatal microbiota and brain dysmaturation

Code for the analyses presented in:

Vaher, K., Blesa Cabez, M., Lusarreta Parga, P., Binkowska, J., Van Beveren, G.J., Odendaal, M.-L., Sullivan, G., Stoye, D.Q., Corrigan, A., Quigley, A.J., Thrippleton, M.J., Bastin, M.E., Bogaert, D., Boardman, J.P., 2023. **The neonatal gut microbiota: a role in the encephalopathy of prematurity**. _medRxiv_. doi: https://doi.org/10.1101/2023.09.12.23295409

Repository author: Kadi Vaher (kadi.vaher@ed.ac.uk)

This repository has been deposited on Zenodo (20/08/2024): 10.5281/zenodo.13347879

## Directory structure
The project assumes the following file directory system:

![image info](dir_structure.png)

The repository consists of the following folders:
- **scripts/** containing R markdown/script files for creating the demographic/clinical variable descriptive tables, performing statistical analyses and creating accompanying figures and tables
- **src/** containing some custom functions used for loading data and creating figures

Note that metadata organisation scripts are not included in this repository; rather we can start with the cleaned and organised metadata.

## Scripts for data analysis

### Sample characteristics
1. microbiome_demotable_paper.Rmd: code for creating study group demographic/clinical data descriptive tables (microbiota dataset)
2. microbiota_mri_demo_paper.Rmd: code for creating study group demographic/clinical data descriptive tables (microbiota-MRI matching dataset)

### 16S rRNA gene sequencing data analysis
3. qc_decontam.Rmd: code for the quality check and decontamination steps for the 16S rRNA sequencing data
4. 16S_beta_diversity.Rmd: code to calculate Bray-Curtis dissimilarity matrix and conduct statistical analysis for clinical drivers of the beta diversity
5. alpha_diversity.Rmd: code to calculate alpha diversity metrics and conduct statistical analysis for clinical drivers of the alpha diversity
6. hclust.Rmd: code for visualising the most abundant ASVs observed in the dataset by using hierarchical clustering to order the samples
7. 16S_relative_abundance.Rmd: code for Maaslin2 models for the clinical covariates

For alpha diversity statistical analysis there is an extra script called "alpha_timepoint_stats_figs.R" which can be used to do timepoint statistics with lmer on the command line if needed.

### Shotgun sequencing data analysis
8. shotgun_tax.Rmd: code for organising shotgun sequencing taxonomy (species-level) data
9. hclust_species.Rmd: code for visualising the most abundant species (shotgun data) observed in the dataset by using hierarchical clustering to order the samples
10. shotgun_tax_stats.Rmd: code for the statistical analysis of shotgun species-level data (PERMANOVAs for beta diversity and Maaslin2 for species-level associations with clinical covariates)
11. calc_gutbrainmodules.Rmd: code to derive gut-brain and gut-metabolic modules based on shotgun sequencing functional data (KEGG)
12. gmm_stats.Rmd: code to perform PERMANOVAs for linking clinical covariates with microbiome functional capacity (GMMs)
13. enterobacteriaceae_klebsiella.Rmd: code to investigate correlations between Klebsiella spp. and Enterobacteriaceae from shotgun and 16S sequencing (not in manuscript)

### Microbiota-brain correlation analyses
14. 16S_ordination.Rmd: code to derive microbiota beta diversity PCo-s, calculate correlations between PCo-s and relative abundances of ASVs, and between the PCo-s and alpha diversity indices
15. microbiota_brain_stats.Rmd: code to perform statistical analyses between brain MRI features and microbiota features (regression models and Maaslin2)
16. brain_16S_figures: code to create figures of the microbiota-brain analysis results from 16S data
17. PCo_shotgun_correlations.Rmd: code to calculate correlations between microbiota community composition PCo-s (derived from 16S data) and species/GBMs from shotgun data
18. species_brain_maaslin.Rmd: code to run Maaslin2 to link brain features (identified as significant from PCo-analyses) and shotgun species-level data
19. gbm_brain_maaslin.Rmd: code to run Maaslin2 to link brain features (identified as significant from PCo-analyses) and shotgun GBM-level functional data
20. gbm_maaslin_heatmap.R: code to make a figure for the GBM-brain Maaslin2 results (heatmap)
21. species_gbms_heatmap.R: code to make a heatmap showing correlations between the abundances of GBMs and top 25 species
22. gbm_species_contribution.Rmd: code to explore contribution of specific species to the brain feature-associated GBMs
23. explore_braindata.Rmd: code to perform regression analysis looking for associations for brain MRI features with GA at birth and GA at scan

### Figures
Scripts for some figures that were not already created in the above scripts

24. permanova_heatmap.Rmd: code to create a heatmap combining PERMANOVA results from 16S ASV-level and shotgun species- and GMM-level data linking clinical covariates with gut microbiome composition
25. maaslin_figure.Rmd: code to create barplot visualising 16S-based data (ASV-level) Maaslin2 results linking clinical covariates with individual bacterial biomarkers
26. figure_panels.R: code to create figures with multiple panels: Fig 1, 2 and Supplementary Fig 2, 4


